var element = document.getElementById('create_table');
element.onclick = function(){
    var existingTables = document.getElementsByTagName('table');
    for(table of existingTables){
        table.remove();
    }

    var table = document.createElement('table');
    for(var i = 0; i < document.getElementById('rows').value; i++){
        var newRow = document.createElement('tr');
        newRow.className = "row";
        for(var j = 0; j < document.getElementById('cols').value; j++){
            var newCol = document.createElement('td');
            newCol.className = "column";
            newRow.appendChild(newCol);
        }
        table.className = "current_table";
        table.appendChild(newRow);
    }
    document.body.appendChild(table);
}